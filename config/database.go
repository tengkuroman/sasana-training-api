package config

import (
	"fmt"
	"os"
	"sasana-training-api/models"
	"sasana-training-api/utils/helpers"

	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func ConnectDatabase() *gorm.DB {
	environment := helpers.GetEnv("ENVIRONMENT", "development")

	if environment == "development" {
		const (
			USERNAME = "root"
			PASSWORD = ""
			HOST     = "tcp(127.0.0.1:3306)"
			DATABASE = "db_sasana_training"
		)

		dsn := fmt.Sprintf("%v:%v@%v/%v?charset=utf8mb4&parseTime=True&loc=Local", USERNAME, PASSWORD, HOST, DATABASE)

		db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
		if err != nil {
			panic(err.Error())
		}

		db.AutoMigrate(&models.Admin{}, models.Course{}, models.Participant{})

		return db
	} else {
		username := os.Getenv("DATABASE_USERNAME")
		password := os.Getenv("DATABASE_PASSWORD")
		host := os.Getenv("DATABASE_HOST")
		port := os.Getenv("DATABASE_PORT")
		database := os.Getenv("DATABASE_NAME")

		dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=require", host, username, password, database, port)

		db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
		if err != nil {
			panic(err.Error())
		}

		db.AutoMigrate(&models.Admin{}, models.Course{}, models.Participant{})

		return db
	}
}
