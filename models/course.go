package models

import (
	"time"

	"gorm.io/gorm"
)

type Course struct {
	gorm.Model
	Name        string
	Start       time.Time
	Duration    int
	Participant []Participant
}
