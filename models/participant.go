package models

import "gorm.io/gorm"

type Participant struct {
	gorm.Model
	FullName     string
	BusinessName string
	Email        string
	PhoneNumber  string
	CourseID     int
}
